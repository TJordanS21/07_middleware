import {useEffect, useState} from "react";

const useRhymeData = (word) => {
    const [someData, setSomeData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);


    useEffect(() => {
        if (word.length > 0) {
            setLoading(true);
            console.log("useeffekt")
            fetch('https://api.datamuse.com/words?rel_rhy=' + word)
                .then(response => response.json())
                .then(data => setSomeData(data))
                .catch(error => setError(error))
                .finally(() => setLoading(false));
        }
    }, [word]);

    return {someData, loading, error};
};

export const RhymeData = ({children, word}) => {
    // DATA FETCHING/MANAGEMENT FRAMEWORK OR LIBRARY OF YOUR CHOICE
    const {someData, loading, error} = useRhymeData(word);
    return children({someData, loading, error});
};